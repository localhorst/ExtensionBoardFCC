EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Extension_Board-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Conn_01x08 J2
U 1 1 59F313A5
P 3000 2950
F 0 "J2" H 3000 3350 50  0000 C CNN
F 1 "1to1" H 3000 2450 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x08_Pitch2.54mm" H 3000 2950 50  0001 C CNN
F 3 "" H 3000 2950 50  0001 C CNN
	1    3000 2950
	-1   0    0    1   
$EndComp
$Comp
L Conn_01x10 J1
U 1 1 59F31402
P 1950 1700
F 0 "J1" H 1950 2200 50  0000 C CNN
F 1 "FPC" H 1950 1100 50  0000 C CNN
F 2 "fpc:fpc-10-1.0mm" H 1950 1700 50  0001 C CNN
F 3 "" H 1950 1700 50  0001 C CNN
	1    1950 1700
	-1   0    0    1   
$EndComp
$Comp
L Conn_01x16 J3
U 1 1 59F314D3
P 4950 1700
F 0 "J3" H 4950 2500 50  0000 C CNN
F 1 "Conn_01x16" H 4950 800 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x16_Pitch1.00mm" H 4950 1700 50  0001 C CNN
F 3 "" H 4950 1700 50  0001 C CNN
	1    4950 1700
	1    0    0    -1  
$EndComp
Text GLabel 2150 1200 2    60   Input ~ 0
VCC
Text GLabel 2150 1300 2    60   Input ~ 0
MISO
Text GLabel 2150 1400 2    60   Input ~ 0
SCK
Text GLabel 2150 1500 2    60   Input ~ 0
MOSI
Text GLabel 2150 1600 2    60   Input ~ 0
RST
Text GLabel 2150 1700 2    60   Input ~ 0
Rx
Text GLabel 2150 1800 2    60   Input ~ 0
Tx
Text GLabel 2150 1900 2    60   Input ~ 0
GND
Text GLabel 3200 2550 2    60   Input ~ 0
GND
Text GLabel 3200 2650 2    60   Input ~ 0
Tx
Text GLabel 3200 2850 2    60   Input ~ 0
RST
Text GLabel 3200 2950 2    60   Input ~ 0
MOSI
Text GLabel 3200 3050 2    60   Input ~ 0
SCK
Text GLabel 3200 3150 2    60   Input ~ 0
MISO
Text GLabel 3200 2750 2    60   Input ~ 0
Rx
Text GLabel 3200 3250 2    60   Input ~ 0
VCC
$Comp
L Conn_01x16 J4
U 1 1 59F316A4
P 5450 1700
F 0 "J4" H 5450 2500 50  0000 C CNN
F 1 "Conn_01x16" H 5450 800 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x16_Pitch1.00mm" H 5450 1700 50  0001 C CNN
F 3 "" H 5450 1700 50  0001 C CNN
	1    5450 1700
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x16 J5
U 1 1 59F316D4
P 5900 1700
F 0 "J5" H 5900 2500 50  0000 C CNN
F 1 "Conn_01x16" H 5900 800 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x16_Pitch1.00mm" H 5900 1700 50  0001 C CNN
F 3 "" H 5900 1700 50  0001 C CNN
	1    5900 1700
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x16 J6
U 1 1 59F31705
P 6350 1700
F 0 "J6" H 6350 2500 50  0000 C CNN
F 1 "Conn_01x16" H 6350 800 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x16_Pitch1.00mm" H 6350 1700 50  0001 C CNN
F 3 "" H 6350 1700 50  0001 C CNN
	1    6350 1700
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x16 J7
U 1 1 59F31747
P 6800 1700
F 0 "J7" H 6800 2500 50  0000 C CNN
F 1 "Conn_01x16" H 6800 800 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x16_Pitch1.00mm" H 6800 1700 50  0001 C CNN
F 3 "" H 6800 1700 50  0001 C CNN
	1    6800 1700
	1    0    0    -1  
$EndComp
Text GLabel 4750 2500 0    60   Input ~ 0
VCC
Text GLabel 5250 2500 0    60   Input ~ 0
VCC
Text GLabel 5700 2500 0    60   Input ~ 0
VCC
Text GLabel 6150 2500 0    60   Input ~ 0
VCC
Text GLabel 6600 2500 0    60   Input ~ 0
VCC
Text GLabel 4750 1000 0    60   Input ~ 0
GND
Text GLabel 5250 1000 0    60   Input ~ 0
GND
Text GLabel 5700 1000 0    60   Input ~ 0
GND
Text GLabel 6150 1000 0    60   Input ~ 0
GND
Text GLabel 6600 1000 0    60   Input ~ 0
GND
$EndSCHEMATC
